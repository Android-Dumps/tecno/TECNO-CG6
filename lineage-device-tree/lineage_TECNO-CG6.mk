#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-CG6 device
$(call inherit-product, device/tecno/TECNO-CG6/device.mk)

PRODUCT_DEVICE := TECNO-CG6
PRODUCT_NAME := lineage_TECNO-CG6
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO CG6
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_cg6_h696-user S SPB1.210331.013 108308 release-keys"

BUILD_FINGERPRINT := TECNO/CG6-OP/TECNO-CG6:S/SPB1.210331.013/UVW-210513V149:user/release-keys
