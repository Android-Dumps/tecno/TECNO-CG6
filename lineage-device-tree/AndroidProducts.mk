#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-CG6.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-CG6-user \
    lineage_TECNO-CG6-userdebug \
    lineage_TECNO-CG6-eng
